# import library socket karena akan menggunakan IPC socket
import socket, pickle, os, time

# definisikan tujuan IP server
ip = "localhost"

# definisikan port dari server yang akan terhubung


# definisikan ukuran buffer untuk mengirimkan pesan
buffer = 1024

# definisikan pesan yang akan disampaikan


# buat socket TCP
s = socket.socket()

# lakukan koneksi ke server dengan parameter IP dan Port yang telah didefinisikan
port = 3578

# kirim pesan ke server


# terima pesan dari server

# tampilkan pesan/reply dari server
s.connect((ip,port))

# definisi update versi pada client
def update():
    app = s.recv(buffer)
    ver = s.recv(buffer)
    app_array = pickle.loads(app)
    versi_punya_server = pickle.loads(ver)
    versi_punya_client = [1,1,1]
    for i in range(len(app_array)):
        print("Aplikasi yang Terinstall",i+1, app_array[i])
    milih = int(input("Pilih untuk lihat versi aplikasi (1/2/3): ", ))
    print("Versi aplikasi",app_array[milih-1],"anda adalah",versi_punya_client[milih-1])
    cek_update = input("ingin melakukan cek update ? (y/n) ")
    if (cek_update == 'y'):
        array_cek = pickle.dumps(versi_punya_client)
        s.send(array_cek)
        cek = s.recv(buffer)
        versi_cek = pickle.loads(cek)
        if (versi_punya_client[milih-1] == versi_cek[milih-1]):
            print("Aplikasi",app_array[milih-1],"Sudah terbaru (versi",versi_punya_client[milih-1],")")
            ack = "ACK-Tidak-Update"
            s.send(ack.encode())
        else:
            apdet = input("Terdapat update aplikasi, ingin update ke versi terbaru ? (y/n)")
            if (apdet == 'y'):
                versi_punya_client[milih-1] = versi_cek[milih-1]
                os.system('cls')
                time.sleep(1)
                print("Sedang update .")
                time.sleep(1)
                os.system('cls')
                print("Sedang update ..")
                time.sleep(1)
                os.system('cls')
                print("Sedang update ...")
                time.sleep(1)
                os.system('cls')
                print("Update berhasil ! \n Aplikasi",app_array[milih-1],"sekarang menjadi versi",versi_punya_client[milih-1])
                ack = "ACK-Update"
                apl = app_array[milih-1]
                vrs = str(versi_punya_client[milih-1])
                s.send(ack.encode())
                s.send(apl.encode())
                time.sleep(1)
                s.send(vrs.encode())
                print("")
                # update()
            else:
                # update()
                pass
    else:
        pass

#update versi terbaru 
update()


# tutup koneksi
s.close()

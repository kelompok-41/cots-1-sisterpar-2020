# COTS 1 SisterPar 2020

# Versioning

----
## Deskripsi Program

Program ini dibuat untuk membantu client melihat update dan update versi terbaru pada aplikasi yang client miliki. Program ini dibuat dengan Interprocess Communication menggunakan socket programming. Pertama client akan melihat aplikasi yang akan diupdate versinya. Setelah itu server akan meng-update versi aplikasi yang dipilih client.


----
## Pembagian Tugas

### Rosmelina Deliani Satrisna

1. membuat seluruh program dari ("ipc_clien")
2. menguji dan mencari kesalahan dari suatu program
3. membuat dokumentasi program

### Mega Ardilla

1. membuat program dari ("ipc_serv")
2. membuat sebagian dokumen readme
3. membuat dokumentasi program

----
## Instalasi Program

* run program server di cmd
* run program client di cmd
* client memilih untuk melihat versi aplikasi
* server menanyakan apakah client akan melakukan cek update
* jika ya, tekan 'y' maka client akan diberi notifikasi oleh server
* server akan notifikasi update-an aplikasi jika ada versi aplikasi yang terbaru
* jika ya, tekan 'y' maka aplikasi pada client ter-update
* jika tidak, tekan 'n' maka program akan tertutup
----

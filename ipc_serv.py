# import library socket karena akan menggunakan IPC socket
import socket
import pickle , time
from datetime import datetime

# definisikan alamat IP binding  yang akan digunakan
ip = "localhost"

# definisikan port number binding  yang akan digunakan
port = 3578

# definisikan ukuran buffer untuk mengirimkan pesan
size = 1024

# buat socket bertipe TCP
s = socket.socket()

# lakukan bind
s.bind((ip,port))

# server akan listen menunggu hingga ada koneksi dari client
s.listen(5)
print("socket is listening")

aplikasi = ["Line", "WhatsApp", "Instagram"]
versi = [1,1,1]

print("LISTENING .. .")


# lakukan loop forever
while 1:
	# menerima koneksi
    c, addr = s.accept()
    # print ('Got connection from', addr)
    print("LISTENING . . .")
	# menampilkan koneksi berupa IP dan port client yang terhubung menggunakan print
    pesan = "soket bind ke alamat" + ip +" dan port " + str(port)
    # print(pesan)
	# menerima data berdasarkan ukuran buffer
    # var = (c.recv(1024).decode())
    # print (var)
    array_app = pickle.dumps(aplikasi)
    c.send(array_app)
    array_convert = pickle.dumps(versi)
    c.send(array_convert)
    cek = c.recv(size)
    server_cek = pickle.loads(cek)
    versi[1] = versi[1] + 1
    versi[2] = versi[2] + 1
    if (versi != server_cek):
        # print(versi)
        send_update = pickle.dumps(versi)
        c.send(send_update)


	# menampilkan pesan yang diterima oleh server menggunakan print
    # print("pesan diterima")
    ack = c.recv(1024).decode()
    if (ack == "ACK-Update"):
        apl = c.recv(1024).decode()
        time_now = datetime.now()
        time_string_format = time_now.strftime("%Y-%b-%d (%H:%M:%S)")
        print("Update Aplikasi oleh user dengan ip : ",addr)
        print("Pada waktu : ", time_string_format)
        print("Nama Aplikasi :",apl)
        vrs = c.recv(1024).decode()
        print("Versi Aplikasi : Versi",vrs,'\n')
    else:
        pass

	# mengirim kembali data yang diterima dari client kepada client
    # c.send(var.encode())

# tutup koneksi
    c.close()
